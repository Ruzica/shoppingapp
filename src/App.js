import 'bootstrap/dist/css/bootstrap.min.css';
import ShoppingCartBuilder from './components/ShoppingCartBuilder/ShoppingCartBuilder';
import ShoppingCart from './components/ShoppingCart/ShoppingCart';
import { Container, Col, Row } from 'react-bootstrap';
import React, { useState } from 'react';
import data from './products.json';
import classes from './App.module.css';

function App() {

  console.log(data);
  const { products } = data;
  const [addedProducts, setAddedProducts] = useState([]);

  const addToCartHandler = (product) => {
    const checkProduct = addedProducts.find(x => x.id === product.id);

    if (checkProduct) {
      setAddedProducts(
        addedProducts.map((x) =>
          x.id === product.id ? { ...checkProduct, numberOfProducts: checkProduct.numberOfProducts + 1 } : x
        )
      );
    } else {
      setAddedProducts([...addedProducts, { ...product, numberOfProducts: 1 }]);
    }

  };

  const removeProductHandler = (product) => {
    const checkProduct = addedProducts.find(x => x.id === product.id);

    if (checkProduct.numberOfProducts === 1) {
      setAddedProducts(addedProducts.filter((x) => x.id !== product.id));
    } else {
      setAddedProducts(
        addedProducts.map((x) =>
          x.id === product.id ? { ...checkProduct, numberOfProducts: checkProduct.numberOfProducts - 1 } : x
        )
      );
    }
  }

  console.log(products);
  console.log('added products:' + addedProducts);

  return (
    <Container className={classes.Containerxxl}>
      <Row>
        <Col className="col-sm-8">
          <ShoppingCartBuilder addToCartHandler={addToCartHandler} products={products} />
        </Col>
        <Col className={classes.ShoppingCart}>
          <ShoppingCart removeProductHandler={removeProductHandler} addToCartHandler={addToCartHandler} addedProducts={addedProducts} />
        </Col>
      </Row>
    </Container>
  );
}

export default App;
