import React from 'react'
import { Col, Card, Button } from 'react-bootstrap';
import classes from './Product.module.css';
import { FaCartPlus } from "react-icons/fa";

export default function Product(props) {
    const { product, addToCartHandler } = props;
    return (
        <Col className="col-sm-4 mb-5">
            <Card className={classes.Card}>
                <Card.Img src={product.image} alt="" />
                <Card.Body>
                    <Card.Text className={classes.Title}>
                        <p>{product.name}</p>
                    </Card.Text>
                    <Card.Text>
                    <div className={classes.Price}>
                        <span>{product.price.amount} {product.price.currency} / <strong>{product.price.measureUnit}</strong></span>
                        </div>
                    </Card.Text>
                    <Button className="btn btn-dark px-4 rounded-pill w-100" onClick={() => addToCartHandler(product)}><FaCartPlus className="mr-2" />Add to Cart</Button>
                </Card.Body>
            </Card>
        </Col>
    )
}
