import React from 'react';
import { Row } from 'react-bootstrap';
import Product from '../Product/Product';

export default function ShoppingCartBuilder(props) {
    const { products, addToCartHandler } = props;
    return (
        <Row>
            {products.map((product) => (
                <Product addToCartHandler={addToCartHandler} key={product.id} product={product}></Product>
            ))}
        </Row>
    );
}