import React from 'react';
import classes from './ShoppingCart.module.css';
import { Row, Col } from 'react-bootstrap';
import { FaTrashAlt } from "react-icons/fa";

export default function ShoppingCart(props) {
    const { addedProducts, addToCartHandler, removeProductHandler } = props;
    const totalPrice = addedProducts.reduce((total, curr) => total + curr.price.amount * curr.numberOfProducts, 0);
    const emptyCart =
        <div style={{ textAlign: "center" }}>
            <h3><strong>Your Cart is Empty</strong></h3>
            <img src="https://i.imgur.com/dCdflKN.png" width="130" height="130" class="img-fluid mb-4 mr-3 test"></img>
        </div>

    return (
        <Col className={classes.Product}>
            <div>
                {addedProducts.map((addedProduct) => (
                    <Row key={addedProduct.id} className="mb-2">
                        <Col className="col-5">
                            <img className={classes.ProductImg} src={addedProduct.image} alt="" />
                        </Col>
                        <Col className="col-7 pt-2">
                            <p className={classes.Title}> {addedProduct.numberOfProducts} x <span>{addedProduct.name}</span></p>
                            <Row>
                                <Col className="col-7">
                                    <strong>Total: <span>{addedProduct.price.amount * addedProduct.numberOfProducts} {addedProduct.price.currency}</span></strong>
                                </Col>
                                <Col className="col-5">
                                    <button className="btn btn-outline-danger btn-xs" onClick={() => removeProductHandler(addedProduct)}><FaTrashAlt /></button>
                                </Col>
                            </Row>
                        </Col>
                        <hr></hr>
                    </Row>
                ))}
                <hr style={{ borderTop: "1px solid #bfa788" }}></hr>
                {addedProducts.length > 0 ? <div> <strong>Total Amount: {totalPrice.toFixed(2)} </strong></div> : emptyCart}
            </div>
        </Col>
    );
}

